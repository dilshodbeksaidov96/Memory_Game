package com.dilshodbek.dragon.lesson35.fragments;

/*
 * Created by MukhammadRasul on 02.06.2018 in Memory_Game
 */

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dilshodbek.dragon.lesson35.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AboutFragment extends Fragment {

    @BindView(R.id.txt2)
    TextView text2;
    @BindView(R.id.txt4)
    TextView text4;
    @BindView(R.id.txt5)
    TextView text5;
    @BindView(R.id.txt6)
    TextView text6;
    @BindView(R.id.txt7)
    TextView text7;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

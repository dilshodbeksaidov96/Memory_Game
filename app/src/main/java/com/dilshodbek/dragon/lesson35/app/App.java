package com.dilshodbek.dragon.lesson35.app;

import android.app.Application;

import com.dilshodbek.dragon.lesson35.database.DataBase;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DataBase.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DataBase.getDataBase().closeDatabase();
    }
}

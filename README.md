MEMORY GAME
===========
****
##MainActivity:
- _Activityning_ `onCreate()` qismida MenuFragmentni fragmentini ochamiz: 
```
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();
        MenuFragment menuFragment = new MenuFragment();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, menuFragment);
        transaction.commit();
        playMusic();
    }
```
- Orqa fonda musiqa qo'yish:
    
``` java    
    private void playMusic() {
        mediaPlayer = MediaPlayer.create(this, R.raw.grasswalk);
        mediaPlayer.start();
    }
```
- Orqa fondagi musiqani pauza qilish:

``` java
    private void pauseMusic() {
        mediaPlayer.pause();
    }
```
- Orqa fondagi musiqani o'chirish:
    
``` java
    private void stopMusic() {
        mediaPlayer.stop();
    }    
``` 
- FragmentManager klasidan fragmentlar bilan ishlash uchun `manager` o'zgaruvchisini olingan;
-------
##Model
> Modelda _**3 ta maydon**_ bor, ular: 
>  +  `imageView` ImageView tipli 
> + `check` boolean tipli;
>+ `imgUrl` long tipli;
     
- Konsruktori yordamida boshlang'ich maydonlarga qiymat beriladi:
 
   - _**2 xil konstruktor**_ mavjud: 
      1. `imageview` va `imgUrl` qabul qiladigan;
      2. faqat `imageview` qabul qiladigan;

``` java
    public PhotosData(ImageView imageView, long imgUrl) {
        this.imageView = imageView;
        this.imgUrl = imgUrl;
    }

    public PhotosData(ImageView imageView) {
        this.imageView = imageView;
    }
 ```

- Model maydonlari qiymatlarini o'qiydigan va ularga qiymat
beradigan _**getter va setter**_ lar mavjud:

``` java
    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public long getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(long imgUrl) {
        this.imgUrl = imgUrl;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
 ```
   ______
   ##MenuFragment
   > MenuFragment o'yinning kirish qismidagi _bosh menyu_ uchun fragment;
 - Fragmentning `onCreateView()` metodini _Override_ qilish orqali fragmentning
ko'rinishini hosil qilinadi: 

 ``` java
  @Nullable
      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
          return inflater.inflate(R.layout.fragment_menu, container, false);
      }
  
 ``` 
  - `onViewCreated()` metodini _Override_ qilib, ko'rinish hosil bo'lganidan keyin **Butterknife**
  kutubxonasi yordamida buttonni topiladi va Unbinder klasi o'zgaruvchisi `unbinder` ga 
  o'zlashtiriladi  
 ``` java
     
     @Override
     public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);
        button.setOnClickListener(this::click);
     }
 ```
  - **`button`** ning bosilishiga `click(View vew)` funksiyasi berilgan:   
```    
     private void click(View view) {
         LevelsFragment levelsFragment = new LevelsFragment();
         FragmentManager manager = getFragmentManager();
         FragmentTransaction transaction = manager.beginTransaction();
         transaction.replace(R.id.container, levelsFragment);
         transaction.commit();
         Toast.makeText(getContext(), "Ishladi", Toast.LENGTH_SHORT).show();
     }
```
- `onDestroyView()` metodini _Override_ qilib, fragmentning yopilish paytid `unbinder` yordamida _bind_ qilingan maydonlarni uzib chiqamiz
``` java   
   @Override
       public void onDestroyView() {
           super.onDestroyView();
           unbinder.unbind();
       }
```
----
##LevelOneFragment
 > 1 - bosqich uchun fregment;

- _Rasmlarni_ ma'lim vaqtdan keyin yopib qoyish:
```
private void hideOriginPhotos() {
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                for (int i = 0; i < images.size(); i++) {
                    hieOriginalPhoto(images.get(i).getImageView());
                }
                setClickViews();
            }
        }.start();
    }
```

- Har bir _rasmni_ yopib qo'yish:
```
    private void hieOriginalPhoto(ImageView imageView) {
        imageView.animate()
                .rotationYBy(90)
                .setDuration(500)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (imageView.getRotationY() < 180) {
                            imageView.setImageResource(R.drawable.market);
                            imageView.animate().rotationYBy(90);
                        } else imageView.setRotationX(0);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
    }
```

- `clickView` larni yuklash:
```
    private void setClickViews() {
        for (int i = 0; i < images.size(); i++) {
            images.get(i).getImageView().setOnClickListener(this::clickView);
        }
    }
```

- Rasmlarni _bosilganda_ chaqiriladigan metod:
```
    private void clickView(View view) {
        for (int i = 0; i < images.size(); i++) {
            if (!images.get(i).isCheck()) images.get(i).getImageView().setEnabled(false);
        }
        if (count == 0) clickViews[0] = (ImageView) view;
        if (count == 1) clickViews[1] = (ImageView) view;
        if (count == 1) checkViews();
        count++;
        view.setRotationY(-180);
        ImageView imageView = (ImageView) view;
        view.animate().rotationYBy(90).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view.getRotationY() < 0) {
                    imageView.setImageResource(imagesDrawableData.get((Integer) view.getTag() / 100));
                    imageView.animate().rotationYBy(90);
                    for (int i = 0; i < images.size(); i++) {
                        if (!images.get(i).isCheck()) images.get(i).getImageView().setEnabled(true);
                        else if (images.get(i).isCheck())
                            images.get(i).getImageView().setEnabled(false);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
```
- 2 ta rasm ochilganda _bir xillikka_ tekhiriladi:
```
    private void checkViews() {
        new CountDownTimer(700, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                count = 0;
            }

            @Override
            public void onFinish() {
                if ((int) clickViews[0].getTag() / 100 == (int) clickViews[1].getTag() / 100) {
                    images.get((Integer) clickViews[0].getTag() % 100).setCheck(true);
                    images.get((Integer) clickViews[1].getTag() % 100).setCheck(true);
                    checkWin();
                } else {
                    hieOriginalPhoto(clickViews[0]);
                    hieOriginalPhoto(clickViews[1]);
                }
                for (int i = 0; i < images.size(); i++) {
                    if (images.get(i).isCheck()) images.get(i).getImageView().setEnabled(false);
                    else if (!images.get(i).isCheck()) {
                        images.get(i).getImageView().setEnabled(true);
                    }
                }
            }
        }.start();
    }
```

- _G'alabaga_ tekshirish:
```
    private void checkWin() {
        int k = 0;
        for (int i = 0; i < images.size(); i++) {
            if (images.get(i).isCheck()) k++;
            else break;
        }
        if (k == images.size()) {
            Toast.makeText(getContext(), "You win", Toast.LENGTH_SHORT).show();
            Toast.makeText(getContext(), "You win", Toast.LENGTH_SHORT).show();
        }
    }
```

- Maydonlarga _ma'lumotlarni_ yuklash:
```
    private void loadDataToViews() {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < images.size(); i++) {
            integers.add(i);
        }
        Collections.shuffle(integers);
        int j = 0;
        for (int i = 0; i < images.size(); i++) {
            int pos = (int) images.get(integers.get(i)).getImageView().getTag();
            images.get(integers.get(i)).getImageView().setTag(j * 100 + pos);
            images.get(integers.get(i)).setImgUrl(imagesDrawableData.get(j));
            images.get(integers.get(i)).getImageView().setImageResource(imagesDrawableData.get(j));
            if (i % 2 == 1) j++;
        }
    }
```

- Rasmlarni ketma-ketligini aralashtirib har doim har xil qilish:
```
    private void shuffleImagesData() {
        Collections.shuffle(imagesDrawableData);
    }
```

- _Maydonlarni_ va boshqa `view` larni yuklash:
```
    private void loadViews() {
        images = new ArrayList<>();
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            viewGroup.getChildAt(i).setTag(i);
            images.add(new PhotosData((ImageView) viewGroup.getChildAt(i)));
        }
    }
```

- *Rasmlarni* yuklash:
```
    private void loadImagesData() {
        imagesDrawableData = new ArrayList<>();
        imagesDrawableData.add(R.drawable.ikon1);
        imagesDrawableData.add(R.drawable.ikon2);
        imagesDrawableData.add(R.drawable.ikon3);
        imagesDrawableData.add(R.drawable.ikon4);
        imagesDrawableData.add(R.drawable.ikon5);
        imagesDrawableData.add(R.drawable.ikon6);
        imagesDrawableData.add(R.drawable.ikon7);
        imagesDrawableData.add(R.drawable.ikon8);
        imagesDrawableData.add(R.drawable.ikon9);
        imagesDrawableData.add(R.drawable.ikon10);
        imagesDrawableData.add(R.drawable.ikon11);
        imagesDrawableData.add(R.drawable.ikon12);
        imagesDrawableData.add(R.drawable.ikon13);
        imagesDrawableData.add(R.drawable.ikon14);
        imagesDrawableData.add(R.drawable.ikon15);
        imagesDrawableData.add(R.drawable.ikon16);
        imagesDrawableData.add(R.drawable.ikon17);
        imagesDrawableData.add(R.drawable.ikon18);
        imagesDrawableData.add(R.drawable.ikon19);
        imagesDrawableData.add(R.drawable.ikon20);
        imagesDrawableData.add(R.drawable.ikon21);
        imagesDrawableData.add(R.drawable.ikon22);
        imagesDrawableData.add(R.drawable.ikon23);
        imagesDrawableData.add(R.drawable.ikon24);
        imagesDrawableData.add(R.drawable.ikon25);
        imagesDrawableData.add(R.drawable.ikon26);
        imagesDrawableData.add(R.drawable.ikon27);
    }
```
- `onDestroyView()` metodini _Override_ qilib, fragmentning yopilish paytid `unbinder` yordamida _bind_ qilingan maydonlarni uzib chiqamiz
``` java   
   @Override
       public void onDestroyView() {
           super.onDestroyView();
           unbinder.unbind();
       }
```

**********

##SimpleDialog_Game
> `SimpleDialog_Game` klasini `AlertDialog` klasidan voris olib yaratiladi. Dialoglar uchun dialogs
nomli package yaratilib shu packagega tashlab qo'yiladi;

- Dialogning _Konstruktori_ yordamida dialogga kerakli ko'rinish beriladi: 
```
    public SimpleDialog_Game(@NonNull Context context) {
        super(context);
        View view= LayoutInflater.from(getContext()).inflate(R.layout.simple_dialog,null,false);
        setView(view);
    }
```

